
snmpv3-write-tests
===================

Created a simple vulnerable setup using amzon linux and the basic commands to test it

Quick Walk through:

* Setup and start the image with vagrant: `vagrant up`
* Install and use SNMP-Write-Check: `git clone https://github.com/DanNegrea/SNMP-Write-Check.git`
* Check the value of the `lo` interface and set it to `up`. If this works, user has write permissions

Setting up a vulnerable server
--------------------------

I used this simple configuration

```bash
# Amazon linux or RHEL
      sudo yum install net-snmp net-snmp-utils net-snmp-devel
      sudo service snmpd stop
      service snmpd stop
      sudo net-snmp-create-v3-user -A testpass -a SHA -x AES testuser
      cp -a /etc/snmp/snmpd.conf /etc/snmp/snmpd.conf.orig
      sudo cat /vagrant/files/snmpd.conf > /etc/snmp/snmpd.conf
      sudo service snmpd start
      sudo chkconfig snmpd on
```

Getting information
------------------

The easiest way to get information is using `snmpwalk`
Here are some examples:

```bash
# Walk all OIDs with different priviledge levels
snmpwalk -v3 -u testuser -l authNoPriv -a SHA -A testpass localhost
snmpwalk -v3 -u testuser -l authPriv -a SHA -A testpass -x AES -X testpass localhost

# Get system.sysContact.0 only
snmpget -v3 -u testuser -l authPriv -a SHA -A testpass -x AES -X testpass localhost system.sysContact.0
```

Validating write access
--------------

The tricky part is fiding OIDs that are writable by default

```bash
# Works for v1 and v2c - bit are not longer writeable in v3
snmpset -v1 -c string localhost system.sysContact.0 s testwriteaccess
snmpset -v1 -c string localhost system.sysLocation.0 s testwriteaccess


$ snmpset -v3 -u testuser -l authPriv -a SHA -A testpass -x AES -X testpass localhost system.sysContact.0 s testwriteaccess
Error in packet.
Reason: notWritable (That object does not support modification)
Failed object: SNMPv2-MIB::sysContact.0

$ snmpset -v3 -u testuser -l authPriv -a SHA -A testpass -x AES -X testpass localhost system.sysLocation.0 s testwriteaccess
Error in packet.
Reason: notWritable (That object does not support modification)
Failed object: SNMPv2-MIB::sysLocation.0
```

Lets check for network interfaces

```bash
# Check if interface lo is up
snmpget -v3 -u testuser -l authPriv -a SHA -A testpass -x AES -X testpass localhost IF-MIB::ifDescr.1 IF-MIB::ifAdminStatus.1
snmpget -v3 -u testuser -l authPriv -a SHA -A testpass -x AES -X testpass localhost 1.3.6.1.2.1.2.2.1.2.1 1.3.6.1.2.1.2.2.1.7.1 

IF-MIB::ifDescr.1 = STRING: lo
IF-MIB::ifAdminStatus.1 = INTEGER: up(1)

# set interface lo to up (same value as before)
snmpset -v3 -u testuser -l authPriv -a SHA -A testpass -x AES -X testpass localhost 1.3.6.1.2.1.2.2.1.7.1 i 1

# Disable eth0
snmpset -v3 -u testuser -l authPriv -a SHA -A testpass -x AES -X testpass localhost 1.3.6.1.2.1.2.2.1.7.3 i 2
```

### Using a script to find writable OIDs

I found only this option: [SNMP-Write-Check](https://github.com/DanNegrea/SNMP-Write-Check.git)

```bash
git clone https://github.com/DanNegrea/SNMP-Write-Check.git
cd SNMP-Write-Check.git
/snmp-write-check.py -v3 -u testuser -l authPriv -a SHA -A testpass -x AES -X testpass localhost
SNMPv2-MIB::sysName.0 is writable
IP-MIB::ipDefaultTTL.0 is writable
IP-MIB::ipv6IpDefaultHopLimit.0 is writable
IP-MIB::ipAddressSpinLock.0 is writable


snmpget -v3 -u testuser -l authPriv -a SHA -A testpass -x AES -X testpass localhost SNMPv2-MIB::sysName.0 
SNMPv2-MIB::sysName.0 = STRING: snmp-test-hostname2
snmpset -v3 -u testuser -l authPriv -a SHA -A testpass -x AES -X testpass localhost SNMPv2-MIB::sysName.0 s snmp-test-hostname2
```

References
------------------

* https://github.com/DanNegrea/SNMP-Write-Check
* http://www.nothink.org/codes/snmpcheck/index.php
* https://book.hacktricks.xyz/pentesting/pentesting-snmp
* https://www.thegeekdiary.com/centos-rhel-6-install-and-configure-snmpv3/
* http://www.net-snmp.org/wiki/index.php/Vacm
* https://docs.fedoraproject.org/en-US/Fedora/17/html/System_Administrators_Guide/sect-System_Monitoring_Tools-Net-SNMP-Configuring.html